<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/cycle')
    ->controller(\App\Http\Controllers\CycleController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{cycle}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
    });
Route::prefix('/invoice')
    ->controller(\App\Http\Controllers\InoviceController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{inovice}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
    });
Route::prefix('/order')
    ->controller(\App\Http\Controllers\OrderController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{order}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
        Route::post('/createorder' , 'createOrder');

    });
Route::prefix('/module')
    ->controller(\App\Http\Controllers\ModulesController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{modules}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
    });
Route::prefix('/product')
    ->controller(\App\Http\Controllers\ProductController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{modules}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
    });
Route::prefix('/service')
    ->controller(\App\Http\Controllers\ServiceController::class)
    ->group(function ()
    {
        Route::get('/','index');
        Route::post('/store' , 'store');
        Route::put('/update/{service}' , 'update');
        Route::delete('/delete/{id}' , 'delete');
    });

