<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cycle extends Model
{
    use SoftDeletes;
    use HasFactory;

    const CYCLE_HOURLY = 'hourly';
    const CYCLE_MONTHLY = 'monthly';
    const CYCLE_ANNUALLY = 'annually';

    protected $fillable=[
        'cycle',
        'amount',
        'product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function InvoiceItem()
    {
        return $this->belongsTo(InvoiceItem::class);
    }
}
