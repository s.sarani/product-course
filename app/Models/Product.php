<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $casts=[
      'active'=>'boolean',
    ];
    protected $fillable=[
        'name',
        'description',
        'active',
        'module_id',
    ];

    public function cycle()
    {
        return $this->hasMany(Cycle::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function InvoiceItem()
    {
        return $this->belongsTo(InvoiceItem::class);
    }
}
