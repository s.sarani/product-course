<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    const STATUS_PENDING = 'pending';
    const EXPIRED_DAYS = 1;
    protected $fillable = [
        'user_id',
        'total_amount',
        'status',
        'paid_at',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function Items()
    {
        return $this->hasMany(InvoiceItem::class);
    }
}
