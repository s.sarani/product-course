<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function cycle()
    {
        return $this->hasOne(Cycle::class);
    }
}
