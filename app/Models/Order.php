<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'total_amount',
        'invoice_id',
        'status',
        'expired_at'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function invocie()
    {
    return $this->hasOne(Invoice::class);
    }
}
