<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'user'=>new UserResource($this->user),
            'product'=>ProductResource::collection($this->product),
            'cycle'=>new CycleResource($this->cycle),
            'status'=>$this->status,
        ];
    }
}
