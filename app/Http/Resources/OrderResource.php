<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user'=>new UserResource($this->user),
            'total_amount'=>$this->total_amount,
            'invocie'=>new InvoiceResource($this->invoice),
            'status'=>$this->status,
        ];
    }
}
