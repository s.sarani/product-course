<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
        'user'=>new UserResource($this->user),
            'amount'=>$this->total_amount,
            'status'=>$this->status,
            'paid_at'=>$this->paid_at,
            'expired_at'=>$this->expired_at,
        ];
    }
}
