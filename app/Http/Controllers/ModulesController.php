<?php

namespace App\Http\Controllers;

use App\Models\Modules;
use App\Repositories\ModuleRepository;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    private  $moduleRepositroy;

    public function __construct()
    {
        $this->moduleRepositroy=new ModuleRepository();
    }

    public function index()
    {
        $this->moduleRepositroy->index();
    }
    public function store(Request $request)
    {
        $data=$request->validate([
           'name'=>'required|string',
        ]);
        $this->moduleRepositroy->create($data);
    }
    public function update(Modules $modules ,Request $request)
    {
        $data=$request->validate([
            'name'=>'required|string',
        ]);
        $this->moduleRepositroy->update($modules , $data);
    }
    public function delete($id)
    {
        $this->moduleRepositroy->delete($id);
    }
}
