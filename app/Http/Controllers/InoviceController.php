<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvoiceResource;
use App\Models\Invoice;
use App\Repositories\InvoiceRepository;
use Illuminate\Http\Request;

class InoviceController extends Controller
{
    private $invoiceRepositroy;

    public function __construct()
    {
        $this->invoiceRepositroy=new InvoiceRepository();
    }

    public function index()
    {
       return InvoiceResource::collection($this->invoiceRepositroy->index());
    }
    public function store(Request $request)
    {
        $data=$request->validate([
            'user_id'=>'required|string',
            'total_amount'=>'required|string',
            'status'=>'required|string',
        ]);
        $this->invoiceRepositroy->create($data);
    }
    public function update(Invoice $inovice , Request $request)
    {
        $data=$request->validate([
            'user_id'=>'required|string',
            'total_amount'=>'required|string',
            'status'=>'required|string',
        ]);
        $this->invoiceRepositroy->update($inovice , $data);
    }
    public function delete($id)
    {
        $this->invoiceRepositroy->delete($id);
    }
}
