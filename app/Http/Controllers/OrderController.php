<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Service;
use App\Processes\OrderProcesses;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $serviceRepositroy;
    private $orderRepositroy;
    private $invocieRepository;

    public function __construct()
    {
        $this->orderRepositroy = new OrderRepository();
        $this->serviceRepositroy = new ServiceRepository();
        $this->invocieRepository = new InvoiceRepository();

    }

    public function index()
    {
        $this->orderRepositroy->index();
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'user_id' => 'required|integer',
            'total_amount' => 'required|integer',
            'invoice_id' => 'required|integer',
            'status' => 'required|integer',
        ]);
        $this->orderRepositroy->create($data);
    }

    public function update(Invoice $inovice, Request $request)
    {
        $data = $request->validate([
            'user_id' => 'required|integer',
            'total_amount' => 'required|integer',
            'invoice_id' => 'required|integer',
            'status' => 'required|integer',
        ]);
        $this->orderRepositroy->update($inovice, $data);
    }

    public function delete($id)
    {
        $this->orderRepositroy->delete($id);
    }

    public function createOrder(Request $request)
    {
        $data = $request->validate([
            'user_id' => 'required|integer',
            'product_ids' => 'required|array',
            'product_id.*'=>'required|exists:products,id',
            'cycle_ids' => 'required|array',
            'cycle_ids.*' => 'required|exists:cycles,id',
        ]);
        $processes=new OrderProcesses($request);
        $processes->run();
    }
}
