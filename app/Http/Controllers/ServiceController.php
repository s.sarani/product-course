<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->serviceRepositroy=new ServiceRepository();
    }

    public function index()
    {
        $this->serviceRepositroy->index();
    }
    public function store(Request $request)
    {
        $data=$request->validate([
            'user_id'=>'required|integer',
            'product_id'=>'required|integer',
            'cycle_id'=>'required|integer',
            'status'=>'required|string',
        ]);
        $this->serviceRepositroy->create($data);
    }
    public function update(Service $service ,Request $request)
    {
        $data=$request->validate([
            'user_id'=>'required|integer',
            'product_id'=>'required|integer',
            'cycle_id'=>'required|integer',
            'status'=>'required|string',
        ]);
        $this->serviceRepositroy->update($service , $data);
    }
    public function delete($id)
    {
        $this->serviceRepositroy->delete($id);
    }
}
