<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private  $productRepositroy;

    public function __construct()
    {
        $this->productRepositroy=new ProductRepository();
    }

    public function index()
    {
        $this->productRepositroy->index();
    }
    public function store(Request $request)
    {
        $data=$request->validate([
            'name'=>'required|string',
            'description'=>'required|string',
            'active'=>'required|integer',
            'module_id'=>'required|integer',
        ]);
        $this->productRepositroy->create($data);
    }
    public function update(Product $product ,Request $request)
    {
        $data=$request->validate([
            'name'=>'required|string',
            'description'=>'required|string',
            'active'=>'required|integer',
            'module_id'=>'required|integer',
        ]);
        $this->productRepositroy->update($product , $data);
    }
    public function delete($id)
    {
        $this->productRepositroy->delete($id);
    }
}
