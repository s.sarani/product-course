<?php

namespace App\Http\Controllers;

use App\Models\Cycle;
use App\Repositories\CycleRepository;
use Illuminate\Http\Request;

class CycleController extends Controller
{
    public function __construct()
    {
        $this->cycleRepositroy=new CycleRepository();
    }

    public function index()
    {

    }
    public function store(Request $request)
    {
     $data=$request->validate([
         'cycle'=>'required|string',
         'amount'=>'required|integer',
         'product_id'=>'required|integer',
     ]);
     $this->cycleRepositroy->create($data);
    }
    public function update(Cycle $cycle , Request $request)
    {
        $data=$request->validate([
            'cycle'=>'required|string',
            'amount'=>'required|integer',
            'product_id'=>'required|integer',
        ]);
        $this->cycleRepositroy->update($cycle , $data);
    }
    public function delete($id)
    {
        $this->cycleRepositroy->delete($id);
    }

}
