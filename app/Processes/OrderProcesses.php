<?php

namespace App\Processes;

use App\Models\Invoice;
use App\Models\Service;
use App\Processes\Order\InvoiceHandler;
use App\Processes\Order\OrderHandler;
use App\Processes\Order\Processes;
use App\Processes\Order\ServiceHandler;
use App\Processes\Order\TotalAmountHandler;
use App\Processes\Order\UpdateInvoiceId;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class OrderProcesses
{
    private Request $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function run()
    {

        $processes=[
          ServiceHandler::class,
          TotalAmountHandler::class,
          OrderHandler::class,
          InvoiceHandler::class,
          UpdateInvoiceId::class,
        ];


        $request = ['request' => $this->request];
        Processes::setData($request);
        collect($processes)->each(function ($process){
           $itemProcess=new $process;
           $itemProcess->handle();
        });
        dd(Processes::getData());
//        $service=new ServiceHandler();
//        $serviceCreate=$service->handle();
//
//        $amount=new TotalAmountHandler($serviceCreate);
//        $totalAmount=$amount->handle();
//
//        $order=new OrderHandler($this->request , $totalAmount);
//        $orderCreate=$order->handle();
//
//        $invoice=new InvoiceHandler($totalAmount, $serviceCreate, $this->request);
//        $invoiceCreate=$invoice->handle();
//
//        $update=new UpdateInvoiceId($orderCreate , $invoiceCreate);
//        $updateInvoice=$invoice->handle();
    }
}
