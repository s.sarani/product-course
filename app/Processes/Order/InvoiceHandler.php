<?php

namespace App\Processes\Order;

use App\Models\Invoice;
use App\Repositories\InvoiceRepository;
use Carbon\Carbon;

class InvoiceHandler extends Processes
{
    private $invocieRepository;

    public function __construct()
    {
        $this->invocieRepository=new InvoiceRepository();
    }

    public function handle()
    {
        $data=Processes::getData();

        $invocie_items=collect($data['services'])->map(function ($item){
            $invoice_item=[
                'service_id'=>$item->id,
                'product_id'=>$item->product_id,
                'cycle_id'=>$item->cycle_id,
                'amount'=>($item->product->cycle->where('id' ,$item->cycle_id)->first())->amount,
            ];
            return $invoice_item;
        });
        $invoice=[
            'user_id'=>$data['request']->user_id,
            'total_amount'=>$data['totalAmount'],
            'status'=>Invoice::STATUS_PENDING,
            'expired_at'=>Carbon::now()->addDays(Invoice::EXPIRED_DAYS)
        ];
        $processInvoice=$this->invocieRepository->create($invoice , $invocie_items);
        $data=['invoice' => $processInvoice];
        Processes::setData($data);
        return $processInvoice;
    }
}
