<?php

namespace App\Processes\Order;

class TotalAmountHandler extends Processes
{


    public function handle()
    {
        $data=Processes::getData();
        $totalAmount=0;
        collect($data['services'])->each(function ($service) use (&$totalAmount){
            $totalAmount+=($service->product->cycle->where('id' ,$service->cycle_id)->first())->amount;
        });
        $data['totalAmount']=$totalAmount;
        Processes::setData($data);
        return $totalAmount ;

    }
}
