<?php

namespace App\Processes\Order;

use function Composer\Autoload\includeFile;

abstract class Processes
{
    private $next;
    private static $data=[];

    /**
     * @return array
     */
    public static function getData()
    {
        return self::$data;
    }

    /**
     * @param array $data
     */
    public static function setData( $data)
    {
        self::$data = $data;
    }
    abstract function handle();

    public function setNext($class)
    {
        return $this->next=$class;
    }

    public function next()
    {
        return $this->next ? true:$this->next->handle();
    }




}
