<?php

namespace App\Processes\Order;

use App\Models\Service;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;


class ServiceHandler extends Processes
{
    public function __construct()
    {
        $this->serviceRepository= new ServiceRepository();
    }

    public function handle()
    {
        $data=Processes::getData();
        $data['services']=collect($data['request']->product_ids)->map(function ($item , $key) use($data,&$totalAmount){
            $service=[
                'user_id'=>$data['request']->user_id,
                'product_id'=>$item,
                'cycle_id'=>$data['request']->cycle_ids[$key],
                'status'=>Service::STATUS_PENDING,
            ];
           return $services=$this->serviceRepository->create($service);
        });
        Processes::setData($data);
    }
}
