<?php

namespace App\Processes\Order;

use App\Models\Invoice;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderHandler extends Processes
{

    private $orderRepositroy;

    public function __construct()
    {
        $this->orderRepositroy=new OrderRepository();
    }

    public function handle()
    {
        $data=Processes::getData();
        $order=[
            'user_id'=>$data['request']->user_id,
            'total_amount'=>$data['totalAmount'],
            'status'=>Invoice::STATUS_PENDING,
        ];
        $orderCreate=$this->orderRepositroy->create($order);
        $data['order']=$orderCreate;
        Processes::setData($data);
        return $orderCreate;
    }
}
