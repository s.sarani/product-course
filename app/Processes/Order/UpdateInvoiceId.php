<?php

namespace App\Processes\Order;

use App\Repositories\OrderRepository;

class UpdateInvoiceId extends Processes
{
    private $orderRepositroy;

    public function __construct()
    {
        $this->orderRepositroy=new OrderRepository();
    }

    public function handle()
    {
        $data=Processes::getData();
        dd($data['invoice']);
        $updateInvoiceId=$this->orderRepositroy->updateInvoiceId($data['order'], [
            'invoice_id'=>$data['invoice']->id,
        ]);
    }
}
