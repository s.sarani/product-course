<?php

namespace App\Repositories;

use App\Models\Modules;

class ModuleRepository
{
    protected  $model;

    public function __construct()
    {
        $this->model=new Modules();
    }
    public function index()
    {
        $this->model->all();
    }
    public function create($data)
    {
        $this->model->create($data);
    }
    public function update($modules , $data)
    {

        return tap($modules)->update($data);
    }
    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }
}
