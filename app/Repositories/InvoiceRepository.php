<?php

namespace App\Repositories;

use App\Models\Invoice;

class InvoiceRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Invoice();
    }

    public function index()
    {
        return $this->model->all();
    }

    public function create($data, $invoice_item)
    {
        $invoice = $this->model->create($data);
        collect($invoice_item)->each(function ($item) use($invoice){
           $invoice->Items()->create($item);
        });
        return $invoice;
    }

    public function update($invoice, $data)
    {

        return tap($invoice)->update($data);
    }

    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }
}
