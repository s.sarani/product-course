<?php

namespace App\Repositories;

use App\Models\Service;

class ServiceRepository
{
    protected $model;

    public function __construct()
    {
        $this->model=new Service();
    }

    public function index()
    {
        $this->model->all();
    }
    public function create($data)
    {
       return $this->model->create($data);
    }
    public function update($service , $data)
    {
        return tap($service)->update($data);
    }
    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }
}
