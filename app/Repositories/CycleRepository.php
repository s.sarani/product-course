<?php

namespace App\Repositories;

use App\Models\Cycle;

class CycleRepository
{
    protected  $model;

    public function __construct()
    {

        $this->model=new Cycle();
    }
    public function index()
    {
        $this->model->all();
    }
    public function create($data)
    {
    $this->model->create($data);
    }
    public function update($cycle , $data)
    {
     return tap($cycle)->update($data);
    }
    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }
}
