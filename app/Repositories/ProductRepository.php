<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    protected $model;

    public function __construct()
    {
        $this->model=new Product();
    }
    public function index()
    {
        $this->model->all();
    }
    public function create($data)
    {
        $this->model->create($data);
    }
    public function update($product , $data)
    {
        return tap($product)->update($data);
    }
    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }

}
