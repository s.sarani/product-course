<?php

namespace Database\Seeders;

use App\Models\Cycle;
use App\Models\Modules;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = ['directadmin', 'cpanel', 'openstack', 'ovh'];
        collect($modules)->each(function ($item) {
            $module = Modules::factory()->create(['name' => $item]);
            $product = Product::factory()->create([
                'module_id' => $module->id
            ]);
            Cycle::factory()->create([
                'product_id' => $product->id,
                'amount'     => round(rand(100, 1000), -2),
                'cycle'      => Cycle::CYCLE_HOURLY,
            ]);
            Cycle::factory()->create([
                'product_id' => $product->id,
                'amount'     => round(rand(100000, 400000), -3),
                'cycle'      => Cycle::CYCLE_MONTHLY,
            ]);
            Cycle::factory()->create([
                'product_id' => $product->id,
                'amount'     => round(rand(500000, 999999), -3),
                'cycle'      => Cycle::CYCLE_ANNUALLY,
            ]);
        });
    }
}
